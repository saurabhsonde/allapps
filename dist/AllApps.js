"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

require("./AllApps.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AllApps = function AllApps(props) {
  var context = props.context;

  var _useState = (0, _react.useState)(""),
      _useState2 = _slicedToArray(_useState, 2),
      search = _useState2[0],
      setSearch = _useState2[1];

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      searchApps = _useState4[0],
      setSearchApps = _useState4[1];

  var apps = [{
    title: "Pexels",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "pexels",
    color: "#00A081"
  }, {
    title: "Unsplash",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "unsplash",
    color: "#000000"
  }, {
    title: "Pixabay",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "pixabay",
    color: "#06BE6D"
  }, {
    title: "Iconscout",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "iconscout",
    color: "#6563FF"
  }, {
    title: "Flaticons",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "flaticons",
    color: "#4AD295"
  }, {
    title: "Stipop",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "stickers",
    color: "#6A7AFF"
  }, {
    title: "Giphy Gifs",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "giphygifs",
    color: "#000000"
  }, {
    title: "Giphy Stickers",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "giphystickers",
    color: "#000000"
  }, {
    title: "IconFinder",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "iconfinder",
    color: "#00CEC9"
  }, {
    title: "Tenor",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "tenor",
    color: "#389DEE"
  }, {
    title: "Captionater",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "captionater",
    color: "#00ECC2"
  }, {
    title: "Quotes",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "quotes",
    color: "#FF8A25"
  }, {
    title: "Hashtag",
    desc: "Lorem Ipsum has been the industry's",
    sideBar: "hashtag",
    color: "#4267B2"
  }];

  var getSearchApp = function getSearchApp() {
    return apps.filter(function (app) {
      return app.title.toLowerCase().includes(search.toLowerCase());
    });
  };

  var appGrid = function appGrid() {
    return _react2.default.createElement(
      "div",
      { className: "appsGrid" },
      apps.map(function (app, index) {
        return _react2.default.createElement(
          "div",
          {
            className: "appCard",
            onClick: function onClick() {
              return context.changeRightSideBar(app.sideBar);
            },
            key: index
          },
          _react2.default.createElement("div", {
            className: "cardImage",
            style: { backgroundColor: app.color }
          }),
          _react2.default.createElement(
            "span",
            null,
            app.title
          ),
          _react2.default.createElement(
            "p",
            null,
            app.desc
          )
        );
      })
    );
  };

  var searchAppGrid = function searchAppGrid() {
    return _react2.default.createElement(
      "div",
      { className: "appsGrid" },
      searchApps.map(function (app, index) {
        return _react2.default.createElement(
          "div",
          {
            className: "appCard",
            onClick: function onClick() {
              return context.changeRightSideBar(app.sideBar);
            },
            key: index
          },
          _react2.default.createElement("div", {
            className: "cardImage",
            style: { backgroundColor: app.color }
          }),
          _react2.default.createElement(
            "span",
            null,
            app.title
          ),
          _react2.default.createElement(
            "p",
            null,
            app.desc
          )
        );
      })
    );
  };

  var handleKeyDown = function handleKeyDown(event) {
    if (event.key) {
      setSearchApps(getSearchApp());
    }
  };

  return _react2.default.createElement(
    "div",
    { className: "allApps" },
    _react2.default.createElement(
      "div",
      { className: "headingApp" },
      _react2.default.createElement(
        "span",
        null,
        "Apps"
      )
    ),
    _react2.default.createElement(
      "div",
      { className: "searchBar" },
      _react2.default.createElement("input", {
        type: "text",
        placeholder: "Search apps",
        onChange: function onChange(e) {
          return setSearch(e.target.value);
        },
        onKeyDown: handleKeyDown
      })
    ),
    search ? searchAppGrid() : appGrid()
  );
};

exports.default = AllApps;
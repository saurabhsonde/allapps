import React, { useState } from "react";
import "./AllApps.css";

const AllApps = (props) => {
  const context = props.context;

  const [search, setSearch] = useState("");
  const [searchApps, setSearchApps] = useState([]);
  const apps = [
    {
      title: "Pexels",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "pexels",
      color: "#00A081",
    },
    {
      title: "Unsplash",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "unsplash",
      color: "#000000",
    },
    {
      title: "Pixabay",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "pixabay",
      color: "#06BE6D",
    },
    {
      title: "Iconscout",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "iconscout",
      color: "#6563FF",
    },
    {
      title: "Flaticons",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "flaticons",
      color: "#4AD295",
    },
    {
      title: "Stipop",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "stickers",
      color: "#6A7AFF",
    },
    {
      title: "Giphy Gifs",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "giphygifs",
      color: "#000000",
    },
    {
      title: "Giphy Stickers",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "giphystickers",
      color: "#000000",
    },
    {
      title: "IconFinder",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "iconfinder",
      color: "#00CEC9",
    },
    {
      title: "Tenor",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "tenor",
      color: "#389DEE",
    },
    {
      title: "Captionater",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "captionater",
      color: "#00ECC2",
    },
    {
      title: "Quotes",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "quotes",
      color: "#FF8A25",
    },
    {
      title: "Hashtag",
      desc: "Lorem Ipsum has been the industry's",
      sideBar: "hashtag",
      color: "#4267B2",
    },
  ];

  const getSearchApp = () => {
    return apps.filter((app) =>
      app.title.toLowerCase().includes(search.toLowerCase())
    );
  };

  const appGrid = () => {
    return (
      <div className="appsGrid">
        {apps.map((app, index) => {
          return (
            <div
              className="appCard"
              onClick={() => context.changeRightSideBar(app.sideBar)}
              key={index}
            >
              <div
                className="cardImage"
                style={{ backgroundColor: app.color }}
              />
              <span>{app.title}</span>
              <p>{app.desc}</p>
            </div>
          );
        })}
      </div>
    );
  };

  const searchAppGrid = () => {
    return (
      <div className="appsGrid">
        {searchApps.map((app, index) => {
          return (
            <div
              className="appCard"
              onClick={() => context.changeRightSideBar(app.sideBar)}
              key={index}
            >
              <div
                className="cardImage"
                style={{ backgroundColor: app.color }}
              />
              <span>{app.title}</span>
              <p>{app.desc}</p>
            </div>
          );
        })}
      </div>
    );
  };

  const handleKeyDown = (event) => {
    if (event.key) {
      setSearchApps(getSearchApp());
    }
  };

  return (
    <div className="allApps">
      <div className="headingApp">
        <span>Apps</span>
      </div>
      <div className="searchBar">
        <input
          type="text"
          placeholder="Search apps"
          onChange={(e) => setSearch(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </div>
      {search ? searchAppGrid() : appGrid()}
    </div>
  );
};

export default AllApps;

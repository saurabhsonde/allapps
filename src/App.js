import React from "react";
import AllApps from "./Components/AllApps";

const App = () => {
  return (
    <div className="App">
      <AllApps />
    </div>
  );
};

export default App;
